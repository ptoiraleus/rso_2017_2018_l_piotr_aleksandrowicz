#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netinet/in.h>
#include <stdbool.h>
#include <string.h>


void root(double a, int socket);
void date(int socket);
bool isLittleEndian();
void swapbytes(double*_object, size_t _size);

int main () {
	while(1){ 
	int sockfd, w;
	socklen_t len;
	struct sockaddr_in address;
	int result;

	/*  Create a socket for the client.  */
	sockfd = socket (AF_INET, SOCK_STREAM, 0);
	/*  Name the socket, as agreed with the server.  */
	address.sin_family = AF_INET;
	address.sin_addr.s_addr = inet_addr ("127.0.0.1");
	address.sin_port = htons (9731);
	len = sizeof (address);
	/*  Now connect our socket to the server's socket.  */
	result = connect (sockfd, (struct sockaddr *) &address,
	len);
	if (result == -1)
	{
		perror ("oops: netclient");
		exit (1);
	} 
		printf("Wybierz cyfre: \n");
		printf("1. Wyswietl czas.\n");
		printf("2. Oblicz pierwiastek.\n");
		printf("3. Koniec.\n");

		scanf("%d",&w);
		switch (w) {
		case 1:
			date(sockfd);
			break;
		case 2:
			double a;
			printf("liczba : ");
			scanf("%lf",&a);
			root(a, sockfd);
			break;
		default:
			close (sockfd);
			exit (0);
			break;
		}
	}
}

bool isLittleEndian(){
	short int number = 0x1;
	char *numPtr = (char*)&number;
	return (numPtr[0] == 1);
}

void swapbytes(double*_object, size_t _size){
	if(isLittleEndian()){
		unsigned char *start, *end;	
		for(start = (unsigned char *)_object, end = start + _size - 1; start < end; ++start, --end) {
			unsigned char swap = *start;
			*start = *end;
			*end = swap;		
		}	
	}
}

void date(int socket) {
	int counter = 0;
	char time[30];
	char *temp;
	write(socket, "02", 2);
	counter = read(socket, time, 30);
	
	temp = time;	

	while(counter != 30){
		temp += strlen(time);
		read(socket, temp, (30 - counter));		
	}
	printf("Current date: %s\n", time);
}

void root(double a, int socket) {	
	double liczba = a;
	double root;
	write(socket, "01", 2);
	sleep(1);
	swapbytes(&a, sizeof(double));
	write(socket, &a, sizeof(a));
	sleep(1);
	read(socket, &root, sizeof(double));
	swapbytes(&root, sizeof(double));
	printf("Root of %lf = %f\n", liczba, root);	
}
