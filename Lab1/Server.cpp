#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <netinet/in.h>
#include <signal.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <stdbool.h>
#include <arpa/inet.h>

#define ROOT 01
#define DATE 02

void date(int socket);
void root(int socket);
bool isLittleEndian();
void swapbytes(double*_object, size_t _size);

int main(){ 

	int serverSocket, clientSocket;
	socklen_t ser_len, cli_len;
	struct sockaddr_in server_address;
	struct sockaddr_in client_address;;
	int orderId;	

	serverSocket = socket(AF_INET,SOCK_STREAM,0);
	
	server_address.sin_family = AF_INET;
	server_address.sin_addr.s_addr = inet_addr ("127.0.0.1");
	server_address.sin_port = htons(9731);
	ser_len = sizeof(server_address);
	
	bind(serverSocket, (struct sockaddr*) &server_address, ser_len);
	
	listen(serverSocket, 10);
	
	while(1){

		char response[10];
				
		printf("Waiting for client...\n");		

		cli_len = sizeof(client_address);
		clientSocket = accept(serverSocket, (struct sockaddr *) &client_address, &cli_len);
		
		read(clientSocket, &response, 2);
		
		orderId = atoi(response);	
		if(orderId == DATE)
			date(clientSocket);
		if(orderId == ROOT)
			root(clientSocket);
	}
	close (clientSocket);
}

bool isLittleEndian(){
	short int number = 0x1;
	char *numPtr = (char*)&number;
	return (numPtr[0] == 1);
}

void swapbytes(double*_object, size_t _size){
	if(isLittleEndian()){
		unsigned char *start, *end;	
		for(start = (unsigned char *)_object, end = start + _size - 1; start < end; ++start, --end) {
			unsigned char swap = *start;
			*start = *end;
			*end = swap;		
		}	
	}
}

void date(int socket){		
	int counter;
	time_t rawtime;
	struct tm * timeinfo;
	char *temp;
	char curr_time[30];	

	time ( &rawtime );
	timeinfo = localtime( &rawtime );
	strcpy(curr_time, ctime(&rawtime));

	counter = write(socket, curr_time, sizeof(curr_time));


}

void root(int socket){
	double a, result;
	read(socket, &a, sizeof(double));
	swapbytes(&a, sizeof(double));
	result = sqrt(a);
	swapbytes(&result, sizeof(double));
	write(socket, &result, sizeof(double));
}
